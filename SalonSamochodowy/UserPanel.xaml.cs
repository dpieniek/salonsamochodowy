﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SalonSamochodowy
{
    /// <summary>
    /// Interaction logic for UserPanel.xaml
    /// </summary>
    public partial class UserPanel : Window
    {
        BazaDanych bd = new BazaDanych();
        public UserPanel()
        {
            InitializeComponent();
        }

        private void buttonUsun_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            UsunUzytkownika usun = new UsunUzytkownika();
            usun.Show();
        }

        private void buttonNowy_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            NowyUzytkownk nowy = new NowyUzytkownk();
            nowy.Show();
        }

        private void buttonWroc_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            MainMenu menu = new MainMenu();
            menu.Show();
        }
    }
}
