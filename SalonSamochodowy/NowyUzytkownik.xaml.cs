﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SalonSamochodowy
{
    /// <summary>
    /// Interaction logic for NowyUzytkownk.xaml
    /// </summary>
    public partial class NowyUzytkownk : Window
    {
        BazaDanych bd = new BazaDanych();
        public NowyUzytkownk()
        {

            InitializeComponent();
        }

        private void buttonStworz_Click(object sender, RoutedEventArgs e)
        {
            bd.zarejstruj(this);
            this.Close();
        }

        private void buttonStworz_Copy_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            UserPanel panel = new UserPanel();
            panel.Show();
        }
        //public static void z
    }
}
