﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SalonSamochodowy
{
    /// <summary>
    /// Interaction logic for UsunUzytkownika.xaml
    /// </summary>
    public partial class UsunUzytkownika : Window
    {
        public BazaDanych bazad = new BazaDanych();
        public UsunUzytkownika()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            bazad.usun(this);
            this.Close();
        }

        private void buttonWroc_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            UserPanel panel = new UserPanel();
            panel.Show(); 
        }
    }
}
